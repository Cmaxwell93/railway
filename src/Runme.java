public class Runme {


    public static void main (String [] args) {

        RailwayTrack track = new RailwayTrack(10);

        Thread statusThread = new Thread(new RailwayStatus(track));
        statusThread.start();

        //Initialize train creator
        TrainCreator makeTrains = new TrainCreator(track);
        Thread makerThread = new Thread(makeTrains);
        makerThread.start();




        System.out.println(track.getNumOfStations() + " stations were created");
        System.out.println(track.getNumOfTracks() + " track segments were created");

        }

    }

