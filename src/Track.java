import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class Track extends RailComponent {

    private int trackNum;
    Train[] trainOnTrack;
    private Lock trainLock;
    private Condition con;

    public Track(int currIndex, int tNum) {
        this.setCapacity(1);
        this.setLength(1000);
        this.setNumOfTrains(0);
        this.trackNum = tNum;
        trainOnTrack = new Train[this.getCapacity()];

        this.spaceAvlCnstrctr(true);
        this.setCompNumber(currIndex);

        trainLock = new ReentrantLock();
        con = trainLock.newCondition();
    }

    public void addTrainToComponent (Train trainToAdd) {
        trainLock.lock();
        boolean stillWaiting = true;
        try {
            while(numOfTrains >= this.getCapacity()) {
                if (!stillWaiting) {
                    Thread.currentThread().interrupt();
                }
                stillWaiting = con.await(5, TimeUnit.SECONDS);
            }
            addTrain(trainToAdd);
        } catch (InterruptedException e) {
            System.out.println("Train "+trainToAdd.getTrainNumber()+" can't move forward\n");
        } finally {
            trainLock.unlock();
        }

        trainToAdd.setLocationObject(this);

    }

    private void addTrain(Train trainToAdd){
        for (int i = 0; i < this.getCapacity(); i++) {
            if (trainOnTrack[i] == null) {
                trainOnTrack[i] = trainToAdd;
                //System.out.println("Train "+trainToAdd.getTrainNumber()+ " was added \n");
                incTrainsInComp();
                this.setSpace();
                break;
            }
        }
    }

    public void removeTrainFromComponent (Train trainToRemove) {
        trainLock.lock();
        for (int i = 0; i < this.getCapacity(); i++) {
            if (trainOnTrack[i] == trainToRemove) {
                trainOnTrack[i] = null;
                decTrainsInComp();
                this.setSpace();
                System.out.println("Train "+trainToRemove.getTrainNumber()+ " was removed from "+this.getCompNumber()+"\n");
                con.signalAll();
                break;
            }
        }
       trainLock.unlock();
    }


    @Override
    public void incTrainsInComp() {
        numOfTrains++;
    }

    @Override
    public void decTrainsInComp() {
        numOfTrains--;
    }

    public void printComp() {

        System.out.println("This is a track segment");

        System.out.println("This is component " + getCompNumber());

        System.out.println("Preceding segment is " + getPrecedingCompNum());

        System.out.println("Next segment is " + getNextCompNum());

        System.out.println("Track capacity is " + getCapacity() + "\n");

        System.out.println("Trains on track: " + numOfTrains + "\n");
    }

    @Override
    public String printCompStatus() {

        String compStatus = new String();
        compStatus = "|--Trk " + trackNum+" Cmp "+getCompNumber()+"--";

        trainLock.lock();

        for (int i = 0; i < trainOnTrack.length; i++) {
            if (trainOnTrack[i] != null) {
                compStatus += (trainOnTrack[i].printTrainStatus() + "--");
            }
        }

        trainLock.unlock();

        compStatus += "--|";


        return compStatus;
    }

    @Override
    public String toString() {
        return "Track";
    }

    public String compString() {
        return "Track "+trackNum;
    }
}
