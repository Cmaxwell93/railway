import java.util.Random;

public class TrainCreator implements Runnable{

    int startLocation ;
    RailwayTrack trackForCreator;
    Random makeDelay = new Random();
    Random trainType = new Random();
    int delayInt;

    public void run() {

        startLocation = 1;

        int trainNum = 1;

        while (true) {

                delayInt = makeDelay.nextInt(1000) + 0;

                try {
                    Thread.sleep((long) delayInt);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.println("TrainCreator slept for " + delayInt + " ms\n");

                //Check there is space on the railway and in first component before creating a train
            if (trackForCreator.getNumOfTrainsActive() < trackForCreator.getNumOfSpacesRailway() && trackForCreator.returnComp(startLocation).isSpaceAvailable()) {

                //Randomly start a slow or fast train
                int trainTypeInt = trainType.nextInt(2);


                if (trainTypeInt == 0) {
                    Thread nTrain = new Thread(new LocalTrain(startLocation, trainNum, trackForCreator));
                    nTrain.start();
                } else {
                    Thread nTrain = new Thread(new ExpressTrain(startLocation, trainNum, trackForCreator));
                    nTrain.start();
                }
                trainNum++;


            }

        }

    }


    public TrainCreator(RailwayTrack mainTrack) {

        trackForCreator = mainTrack;

    }
}
