import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class Station extends RailComponent {

    private int stationNum;
    private Train[] trainsInStation;
    private Lock trainLock;
    private Condition con;

    public Station(int currIndex, int sNum, int capacitySetter) {

        this.setCapacity(capacitySetter);
        this.setLength(100);
        this.setNumOfTrains(0);
        this.stationNum = sNum;
        trainsInStation = new Train[this.getCapacity()];
        this.spaceAvlCnstrctr(true);
        this.setCompNumber(currIndex);

        System.out.println("Station "+stationNum+" capacity set as " + capacitySetter +"\n");
        trainLock = new ReentrantLock();
        con = trainLock.newCondition();
    }

    public void addTrainToComponent (Train trainToAdd) {
        trainLock.lock();
        boolean stillWaiting = true;
        try {
            while(numOfTrains >= this.getCapacity()) {
                if (!stillWaiting) {
                    Thread.currentThread().interrupt();
                }

                stillWaiting = con.await(5, TimeUnit.SECONDS);
            }
            addTrain(trainToAdd);
        } catch (InterruptedException e) {
            System.out.println("Train "+trainToAdd.getTrainNumber()+" can't move forward\n");
        } finally {
            trainLock.unlock();
        }

        trainToAdd.setLocationObject(this);

    }

    private void addTrain(Train trainToAdd){
        for (int i = 0; i < this.getCapacity(); i++) {
            if (trainsInStation[i] == null) {
                trainsInStation[i] = trainToAdd;
                //System.out.println("Train "+trainToAdd.getTrainNumber()+ " was added \n");
                incTrainsInComp();
                this.setSpace();
                break;
            }
        }
    }

    public void removeTrainFromComponent (Train trainToRemove) {
        trainLock.lock();
        for (int i = 0; i < this.getCapacity(); i++) {
            if (trainsInStation[i] == trainToRemove) {
                trainsInStation[i] = null;
                decTrainsInComp();
                this.setSpace();
                System.out.println("Train "+trainToRemove.getTrainNumber()+ " was removed from "+this.getCompNumber()+"\n");
               con.signalAll();
               break;
            }
        }
        trainLock.unlock();
    }


    @Override
    public void incTrainsInComp() {
        numOfTrains++;
    }

    @Override
    public void decTrainsInComp() {
        numOfTrains--;
    }

    @Override
    public String printCompStatus() {

        String compStatus = new String();
        compStatus = "|--Stn "+stationNum+" Cmp "+getCompNumber()+" ("+numOfTrains+"/"+capacity+")--";

        trainLock.lock();
        for (int i = 0; i < trainsInStation.length; i++) {
            if (trainsInStation[i] != null) {
                compStatus += (trainsInStation[i].printTrainStatus() + "--");
            }
        }

        trainLock.unlock();

        compStatus+= "--|";

        return compStatus;
    }

    @Override
    public String toString() {
        return "Station";
    }

    public String compString() {
        return "Station "+stationNum;
    }
}
