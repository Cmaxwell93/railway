public class ExpressTrain extends Train implements Runnable {


    private int moveCount = 0;

    public ExpressTrain(int startLocation, int trainNumber, RailwayTrack t) {
        super(t);
        this.setSpeed(500);
        this.setLocation(startLocation);
        this.setNextDest(startLocation);
        this.setTrainNumber(trainNumber);
        this.locationObject = t.returnComp(getLocation());
        t.returnComp(getLocation()).addTrainToComponent(this);
        this.track = t;

        t.incNumOfTrains();
        System.out.println("There are currently "+t.getNumOfTrainsActive()+" trains active\n");

    }

    public void run() {


                while (isRunning) {

                    timeInSegment = (locationObject.getLength()/this.getSpeed())*1000;
                    System.out.print("Train "+getTrainNumber()+" moves at "+this.getSpeed()+" m/s\n");

                    if (locationObject.toString() == "Station") {
                        timeInSegment+= 5000;
                        System.out.println("Train "+this.getTrainNumber()+" is in "+locationObject.toString()+" "+locationObject.getCompNumber()+" for "+timeInSegment+" ms\n");
                    }
                    else {
                        System.out.println("Train "+this.getTrainNumber()+" is in "+locationObject.toString()+" "+locationObject.getCompNumber()+" for "+timeInSegment+" ms\n");
                    }




                    try {
                        Thread.sleep((long)timeInSegment);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    try {
                            moveTrain();

                    } finally {
                        moveCount++;
                        System.out.println("Train " + this.getTrainNumber() + " has moved " + moveCount + " times\nTrain "+this.getTrainNumber()+" is at component "+this.getLocation());
                    }
            }


    }


    @Override
    public String printTrainStatus() {

        String trainPrint = "T" + getTrainNumber();

        return trainPrint;
    }
}
