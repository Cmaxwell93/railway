public class Train {

    private double speed;
    private int location;
    protected RailComponent locationObject;
    private int nextDest;
    private int trainNumber;
    protected double timeInSegment;
    protected RailwayTrack track;
    protected boolean isRunning = true;
    private String trainPrint;

    public Train(RailwayTrack t) {
        this.track = t;
    }

    public void setLocationObject(RailComponent locationObjectSetter) {
        this.locationObject = locationObjectSetter;
    }

    public void setTrainNumber(int trainNumber) {
        this.trainNumber = trainNumber;
    }

    public int getTrainNumber() {
        return this.trainNumber;
    }

    public void setSpeed(long speed) {
        this.speed = speed;
    }

    public double getSpeed() {
        return this.speed;
    }

    public void setLocation(int location) {
        this.location = location;
    }

    public int getLocation() {
        return this.location;
    }

    public void setNextDest(int location) {
            this.nextDest = track.returnComp(location).getNextCompNum();
    }

    public int getNextDest() {
        return this.nextDest;
    }

    public void removeTrain() {

        int startLocation = this.getNextDest();
        RailComponent startComp = track.returnComp(startLocation);
        startComp.removeTrainFromComponent(this);
        startComp.decTrainsInComp();
        startComp.setSpace();
    }

    public void addTrain() {

        int finLocation = this.nextDest;
        RailComponent finComp = track.returnComp(finLocation);

        if (finLocation == 1) {
            track.accessGraveyard().add(this);
            this.isRunning = false;
            track.decNumOfTrains();
            System.out.println("Train "+this.getTrainNumber()+" reached end of line and retired\n");
        }

        else {
            finComp.addTrainToComponent(this);
            finComp.incTrainsInComp();
            finComp.setSpace();
            this.setLocation(finLocation);
            this.setNextDest(finLocation);
        }
    }

    //Moves Train from current location to the next when space is available
    public void moveTrain() {
        removeTrain();
        addTrain();
        System.out.println("There are " + locationObject.numOfTrains + "/"+locationObject.capacity+" trains in " + locationObject.compString());
    }

    public String printTrainStatus() {

        trainPrint = "T" + trainNumber;

        return trainPrint;
    }

    @Override
    public String toString() {
        String trainAsString = new String();
        trainAsString = "This is Train " + trainNumber + "\nIt is at location " + location + "\nNext destination is " + getNextDest() + "\n";
        return trainAsString;
    }

}
