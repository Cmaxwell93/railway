import java.util.ArrayList;
import java.util.Random;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class RailwayTrack {

    private int sizeOfTrack;
    private int numOfStations;
    private int numOfTracks;
    private int numOfTrainsActive;
    private int numOfSpacesRailway;
    private ArrayList<RailComponent> rTrack = new ArrayList<>();
    private ArrayList<Train> deadTrains = new ArrayList<Train>();
    private Lock printLock;
    private Condition con;

    public ArrayList<Train> accessGraveyard() {
        return deadTrains;
    }

    public RailwayTrack(int size) {

        printLock = new ReentrantLock();
        con = printLock.newCondition();

        numOfStations = 0;
        numOfTracks = 0;

        Random trackOrStation = new Random();
        Random capacitySetter = new Random();

        sizeOfTrack = size;

        for (int i = 0; i < sizeOfTrack; i++) {

            //randomly create a mix of stations and track segments

            int trackOrStat = trackOrStation.nextInt(2);


            if (trackOrStat == 1) {

                if (rTrack.size() > 0 && rTrack.get(i - 1).toString() == "Station") {
                    createTrack(i);
                } else {

                    createStation(i, capacitySetter);
                }
            } else {
                //print value of random integer
                createTrack(i);

            }
        }


        setUpOrder();

        int numOfSpaces = 0;

        for (int j = 0; j < sizeOfTrack; j++) {
            numOfSpaces += rTrack.get(j).capacity;
        }

        setNumOfSpacesRailway(numOfSpaces);
        System.out.println("There are " + getNumOfSpacesRailway() + " spaces for trains across all components of the railway\n");

    }

    public void createTrack(int i) {
        //create Track component
        numOfTracks++;
        RailComponent railComp = new Track(i, numOfTracks);
        rTrack.add(railComp);


        //if RailwayTrack already contains components, sets the current components preceding component
        if (rTrack.size() > 1) {
            rTrack.get(i).setPrecedingCompNum((rTrack.get(i - 1)).getCompNumber());
        }
    }

    public void createStation(int i, Random capacitySetter) {


        //create a Station, supply current index as parameter to set number of component, and random integer to set capacity
        int stationCap = capacitySetter.nextInt(3) + 1;
        numOfStations++;
        RailComponent railComp = new Station(i, numOfStations, stationCap);
        rTrack.add(railComp);

        //if RailwayTrack already contains components, sets the current components preceding component
        if (rTrack.size() > 1) {
            rTrack.get(i).setPrecedingCompNum((rTrack.get(i - 1)).getCompNumber());
            rTrack.get(i).setPreCompData((rTrack.get(i - 1)));
        }
    }

    public void setUpOrder() {
        //Populate value of nextComponent for all components of railway system
        for (int j = 0; j < sizeOfTrack - 1; j++) {
            rTrack.get(j).setNextCompNum((rTrack.get(j + 1)).getCompNumber());
            rTrack.get(j).setNextCompData(rTrack.get(j + 1));
        }

        //Set value of nextComponent and precedingComponent for the beginning and end components
        rTrack.get(0).setPrecedingCompNum((rTrack.get(sizeOfTrack - 1)).getCompNumber());
        rTrack.get(sizeOfTrack - 1).setNextCompNum((rTrack.get(0)).getCompNumber());

        rTrack.get(0).setPreCompData(rTrack.get(sizeOfTrack - 1));
        rTrack.get(sizeOfTrack - 1).setNextCompData(rTrack.get(0));
    }



    public int getSize() {
        return this.sizeOfTrack;
    }

    public RailComponent returnComp(int location) {
        return rTrack.get(location - 1);
    }

    public int getNumOfStations() {
        return this.numOfStations;
    }

    public int getNumOfTracks() {
        return this.numOfTracks;
    }

    public int getNumOfTrainsActive() {
        return this.numOfTrainsActive;
    }

    public void incNumOfTrains() {
        this.numOfTrainsActive++;
    }


    public void decNumOfTrains() {
        this.numOfTrainsActive--;
    }

    public int getNumOfSpacesRailway() {
        return this.numOfSpacesRailway;
    }

    public void setNumOfSpacesRailway(int numOfSpaces) {
        this.numOfSpacesRailway = numOfSpaces;
    }

}


