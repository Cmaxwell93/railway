public class RailwayStatus implements Runnable {


    /*Class prints all station and track components of railway track, including trains present in the components
    and how full it is if a station. Does not consistently display the removal of one train, though it does show
    addition to the next component. I think this is a problem with the RailwayStatus thread rather than the methods for
    adding and removing trains.
    */


    private RailwayTrack trackForStatus;

    public RailwayStatus(RailwayTrack track) {
        this.trackForStatus = track;
    }

    public void run() {

        while (true) {
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            String statusPrint = "";

            for (int i = 1; i <= trackForStatus.getSize(); i++) {
                statusPrint += trackForStatus.returnComp(i).printCompStatus();
            }

            System.out.println(statusPrint+"\n");
        }
    }
}
