public abstract class RailComponent {

    private double length;
    protected int capacity;
    protected int numOfTrains;
    private boolean spaceAvailable;
    private int precedingCompNum;
    private int nextCompNum;
    private RailComponent preCompData;
    private RailComponent nextCompData;
    private int compNumber;

    public void setLength(double length) {
        this.length = length;
    }

    public double getLength () {
        return this.length;
    }

    public void setCapacity(int capacity) {
        this.capacity = capacity;
    }

    public int getCapacity() {
        return this.capacity;
    }

    public void setNumOfTrains(int numOfTrains) {
        this.numOfTrains = numOfTrains;
    }

    public void spaceAvlCnstrctr(boolean spaceAvailable) {
        this.spaceAvailable = spaceAvailable;
    }

    public void setSpace() {
        if (this.numOfTrains < this.getCapacity()) {
            spaceAvailable = true;
        }
        else spaceAvailable = false;
    }

    public abstract void incTrainsInComp();

    public abstract void decTrainsInComp();

    public boolean isSpaceAvailable() {
        return this.spaceAvailable;
    }


    public void setPrecedingCompNum(int precedingComponent) {
        this.precedingCompNum = precedingComponent;
    }

    public int getPrecedingCompNum() {
        return this.precedingCompNum;
    }

    public void setNextCompNum(int nextComponent) {
        this.nextCompNum = nextComponent;
    }

    public int getNextCompNum() {
        return nextCompNum;
    }

    public void setPreCompData(RailComponent precedingCompData) {
        this.preCompData = precedingCompData;
    }


    public void setNextCompData(RailComponent succeedingCompData) {
        this.nextCompData = succeedingCompData;
    }


    public void setCompNumber(int currIndex) {
        this.compNumber = currIndex+1;
    }

    public int getCompNumber() {
        return this.compNumber;
    }

    public abstract void addTrainToComponent (Train trainToAdd);

    public abstract void removeTrainFromComponent (Train trainToRemove);

    public abstract String printCompStatus();

    public abstract String compString();

}



